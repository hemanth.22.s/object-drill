function invert(obj){
    if(obj == undefined){
        return []
    } else {
        let output = {}

        for (let item in obj){
            output[obj[item]] = item
        }

        return output;
    }
}

module.exports = invert;
