function defaults(obj, defaultProps){
    if(obj == undefined || defaultProps == undefined){
        return [];
    } else {
        let output = {}

        for(let item in defaultProps){
            if(obj!=[item]){
                output[item] = defaultProps[item]
            }
            else{
                output[item] = obj[item]
            }
        }

        return output
    }
}

module.exports = defaults;

// function defaults(obj, defaultProps){
//     return {...obj, ...defaultProps}
// }

module.exports = defaults;