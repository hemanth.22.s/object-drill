// const defaultProps = require("../defaults");

// const testObject = { name: 'Bruce Wayne'};

// const result = defaultProps(testObject,{name:'Hemanth'});

// console.log(result);

const defaults = require("../defaults")

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const newObject = {name: "Hemanth", age: 24, location: 'Bangalore'};

const result = defaults(testObject, newObject)

console.log(result);