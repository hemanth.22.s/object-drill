function pairs(obj){
    if(obj == undefined){
        return []
    }
    else{

        let output = []

        for(let item in obj){
            output.push([item,obj[item]])
        }

        return output;
    }

}

module.exports = pairs;
